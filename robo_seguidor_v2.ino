#include <SoftwareSerial.h>

// variavel para realizar debug no programa atraves da entrada serial (usando no serial monitor)
bool debug = true;

// velocidade comunicacao Serial
#define velocidadeSerial 9600

// esquerda
#define pinSensorA 6
#define pinSentido1MotorA 2
#define pinSentido2MotorA 3

// direita
#define pinSensorB 7
#define pinSentido1MotorB 4
#define pinSentido2MotorB 5

// configuracao bluetooth
#define pinBtRX 8
#define pinBtTX 9

SoftwareSerial bt(8, 9);

bool isFalha = false;
int contadorFalha = 0;
int velocidadeRobo = 300;
int quantidadeVoltas = 0;
int quantidadeFalhasConsideradas = 0;

void setup() {
  Serial.begin(velocidadeSerial);
  bt.begin(velocidadeSerial);
  
  pinMode(pinSensorA, INPUT);
  pinMode(pinSentido1MotorA, OUTPUT);
  pinMode(pinSentido2MotorA, OUTPUT);

  pinMode(pinSensorB, INPUT);
  pinMode(pinSentido1MotorB, OUTPUT);
  pinMode(pinSentido2MotorB, OUTPUT);
  
  pinMode(pinBtRX, INPUT);
  pinMode(pinBtTX, OUTPUT);
}

void loop() {
  isFalha = verificarFalha(contadorFalha);
  if(!isFalha){
    debugSerial("circuito sem falhas...");
    bool estadoSensorA = digitalRead(pinSensorA);
    debugSerial("estadoSensorA: " + String(estadoSensorA));
    bool estadoSensorB = digitalRead(pinSensorB);
    debugSerial("estadoSensorB: " + String(estadoSensorB));
      
    if(!estadoSensorA && !estadoSensorB){
      debugSerial("Sensor A e B nao detectou barreira preta");
      andarParaFrente(70);
      pararRobo();
      contadorFalha += 1;
      if(quantidadeVoltas == 0){
        quantidadeFalhasConsideradas += 1;  
      }
      debugSerial("quantidadeFalhasConsideradas: " + String(quantidadeFalhasConsideradas));
      debugSerial("contadorFalha: " + String(contadorFalha));
      debugSerial("quantidadeVoltas: " + String(quantidadeVoltas));
    }
  
    if(estadoSensorA && estadoSensorB){
      debugSerial("Sensor A e B detectou barreira preta");
      //andarParaTras(70);
      andarParaFrente(70);
      pararRobo();
      contadorFalha = 0;
      quantidadeVoltas +=1;
      velocidadeRobo = 80;
    }
  
    if(estadoSensorA && !estadoSensorB){  
      debugSerial("Sensor A detectou barreira preta");
      andarParaEsquerda(100);
      pararRobo();
      contadorFalha = 0;
    }
  
    if(!estadoSensorA && estadoSensorB){  
      debugSerial("Sensor B detectou barreira preta");
      andarParaDireita(100);
      pararRobo();
      contadorFalha = 0;
    }
     delay(velocidadeRobo);
  }else{
   
    debugSerial("atencao circuito em falha favor verificar!");
  }
}

void andarParaFrente(int tempo){
  debugSerial("andando para frente...");
  digitalWrite(pinSentido1MotorA, LOW);
  digitalWrite(pinSentido2MotorA, HIGH);
  digitalWrite(pinSentido1MotorB, LOW);
  digitalWrite(pinSentido2MotorB, HIGH);
  delay(tempo);
}

void andarParaTras(int tempo){
  debugSerial("andando para tras...");
  digitalWrite(pinSentido1MotorA, HIGH);
  digitalWrite(pinSentido2MotorA, LOW);
  digitalWrite(pinSentido1MotorB, HIGH);
  digitalWrite(pinSentido2MotorB, LOW);
  delay(tempo);
}

void andarParaEsquerda(int tempo){
  debugSerial("andando para esquerda...");
  digitalWrite(pinSentido1MotorB, LOW);
  digitalWrite(pinSentido2MotorB, HIGH);
  digitalWrite(pinSentido1MotorA, HIGH);
  digitalWrite(pinSentido2MotorA, LOW);
  delay(tempo);
}

void andarParaDireita(int tempo){
  debugSerial("andando para direita...");
  digitalWrite(pinSentido1MotorA, LOW);
  digitalWrite(pinSentido2MotorA, HIGH);
  digitalWrite(pinSentido1MotorB, HIGH);
  digitalWrite(pinSentido2MotorB, LOW);
  delay(tempo);
}

void pararRobo(){
  debugSerial("robo parado!");
  digitalWrite(pinSentido1MotorA, LOW);
  digitalWrite(pinSentido2MotorA, LOW);
  digitalWrite(pinSentido1MotorB, LOW);
  digitalWrite(pinSentido2MotorB, LOW);
}

bool verificarFalha(int contadorFalha){
  debugSerial("verificando falhas...");
  if(contadorFalha > quantidadeFalhasConsideradas && quantidadeVoltas > 0){
    debugSerial("falha encontrada!");
    return true;
  }else{
    debugSerial("nenhuma falha encontrada!");
    return false;
  }
}

void debugSerial(String mensagem){
  if(debug){
    Serial.print("debug: ");
    Serial.println(mensagem);
    bt.print("debug: ");
    bt.println(mensagem);  
  }
}
